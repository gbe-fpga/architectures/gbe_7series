#
####
#######
##########
#############
#################
#EXAMPLE DESIGN CONSTRAINTS


############################################################
# TX Clock period Constraints                              #
############################################################
# Transmitter clock period constraints: please do not relax
 
set axi_clk_name [get_clocks -of_objects [get_pins example_clocks/bufg_axi_clk/O]]


############################################################
# Ignore paths to resync flops
############################################################
set_false_path -to [get_pins -hier -filter {NAME =~ */reset_sync*/PRE}]
