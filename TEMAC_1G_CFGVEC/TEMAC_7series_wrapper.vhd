library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;


entity TEMAC_7series_wrapper is
  generic (
    RX_FC_EN : std_logic := '0';
    TX_FC_EN : std_logic := '0'
    );
  port (
    reset                : in  std_logic;
    gtx_clk              : in  std_logic;
    link_valid_in        : in  std_logic;
    -- MAC control interface
    mac_addr_in          : in  std_logic_vector(47 downto 0);
    mac_speed            : in  std_logic_vector(1 downto 0) := "10";
    update_speed         : in  std_logic                    := '0';
    pause_req            : in  std_logic;
    pause_val            : in  std_logic_vector(15 downto 0);
    -- MAC status/config info
    speedis100           : out std_logic;
    speedis10100         : out std_logic;
    mac_rx_cfg_vec_out   : out std_logic_vector(79 downto 0);
    mac_tx_cfg_vec_out   : out std_logic_vector(79 downto 0);
    mac_active_out       : out std_logic;
    -- MAC RX interface
    rx_statistics_vector : out std_logic_vector(27 downto 0);
    rx_statistics_valid  : out std_logic;
    rx_mac_aclk          : out std_logic;
    rx_mac_tdata         : out std_logic_vector(7 downto 0);
    rx_mac_tvalid        : out std_logic;
    rx_mac_tlast         : out std_logic;
    rx_mac_tuser         : out std_logic;
    -- TX interface (through FIFO)
    tx_ifg_delay         : in  std_logic_vector(7 downto 0);
    tx_statistics_vector : out std_logic_vector(31 downto 0);
    tx_statistics_valid  : out std_logic;
    tx_fifo_full_out     : out std_logic;
    tx_fifo_data_in      : in  std_logic_vector(8 downto 0);
    tx_fifo_we_in        : in  std_logic;
    --reset_ctr            : in  std_logic;
    --fr_ctr_out           : out std_logic_vector(15 downto 0);
    -- GMII interface to PHY
    gmii_txd             : out std_logic_vector(7 downto 0);
    gmii_tx_en           : out std_logic;
    gmii_tx_er           : out std_logic;
    gmii_rxd             : in  std_logic_vector(7 downto 0);
    gmii_rx_dv           : in  std_logic;
    gmii_rx_er           : in  std_logic
    );
end TEMAC_7series_wrapper;

architecture wrapper of TEMAC_7series_wrapper is

  -- attribute DowngradeIPIdentifiedWarnings            : string;
  -- attribute DowngradeIPIdentifiedWarnings of wrapper : architecture is "yes";

  ------------------------------------------------------------------------------
  -- Internal signals used in this wrapper
  ------------------------------------------------------------------------------

  signal resetn             : std_logic;
  signal rx_reset, tx_reset : std_logic;

  -- Configuration vectors
  signal rx_configuration_vector : std_logic_vector(79 downto 0);
  signal tx_configuration_vector : std_logic_vector(79 downto 0);
  signal mac_running             : std_logic;

  -- TX interface
  -- signal tx_fifo_tdata  : std_logic_vector(7 downto 0);
  -- signal tx_fifo_tvalid : std_logic;
  -- signal tx_fifo_tlast  : std_logic;
  -- signal tx_fifo_tready : std_logic;
  -- signal tx_fifo_tuser  : std_logic;

  type tx_state_t is (IDLE, PREPARE, TRANSMITTING, DELAY, FIFO_RESET);
  signal tx_state      : tx_state_t           := IDLE;
  signal next_state    : tx_state_t;
  signal state_v       : std_logic_vector(3 downto 0);
  signal dly_ctr       : integer range 0 to 7 := 0;
  signal dly_ctr_set   : integer range 0 to 7 := 0;
  signal dly_ctr_init  : std_logic            := '0';
  signal dly_exp       : std_logic;
  signal tx_fifo_reset : std_logic;
  signal tx_fifo_we    : std_logic;
  signal tx_fifo_rd_en : std_logic;
  signal tx_fifo_q     : std_logic_vector(8 downto 0);
  signal tx_fifo_full  : std_logic;
  signal tx_fifo_mpty  : std_logic;
  signal tx_fifo_ampty : std_logic;

  signal tx_mac_aclk   : std_logic;
  signal tx_mac_tdata  : std_logic_vector(7 downto 0);
  signal tx_mac_tvalid : std_logic;
  signal tx_mac_tlast  : std_logic;
  signal tx_mac_tready : std_logic;
  signal tx_mac_tuser  : std_logic_vector(0 downto 0);

  --signal dbg_ctr : unsigned(15 downto 0) := (others => '0');

------------------------------------------------------------------------------
-- Begin architecture
------------------------------------------------------------------------------
begin

------------------------------------------------------------------------------
-- Reset signals
------------------------------------------------------------------------------

  resetn <= not reset;

-------------------------------------------------------------------------------
-- TX FIFO interface
-------------------------------------------------------------------------------
  
  MAC_TX_FIFO : entity work.FIFO_CCBI_W9_D16k
    port map (
      clk        => gtx_clk,
      rst        => tx_fifo_reset,
      din        => tx_fifo_data_in,
      wr_en      => tx_fifo_we,
      rd_en      => tx_fifo_rd_en,
      dout       => tx_fifo_q,
      full       => open,
      empty      => tx_fifo_mpty,
      underflow  => tx_mac_tuser(0),
      prog_full  => tx_fifo_full,
      prog_empty => tx_fifo_ampty
      );

  tx_fifo_full_out <= tx_fifo_full or tx_fifo_reset;
  tx_fifo_we       <= tx_fifo_we_in and not tx_fifo_reset;
  tx_mac_tdata     <= tx_fifo_q(7 downto 0);
  tx_mac_tlast     <= tx_fifo_q(8);

  tx_fifo_rd_en <= '1' when tx_state = PREPARE else
                   '1' when tx_state = TRANSMITTING and tx_mac_tready = '1' and not tx_mac_tlast = '1' else
                   '0';

  tx_mac_tvalid <= '1' when tx_state = TRANSMITTING else '0';

  tx_fifo_reset <= '1' when tx_state = FIFO_RESET else '0';


  -- debug
  -- process(gtx_clk)
  -- begin
  --   if rising_edge(gtx_clk) then
  --     if reset = '1' or reset_ctr = '1' then
  --       dbg_ctr <= (others => '0');
  --     else
  --       if tx_state = TRANSMITTING and tx_mac_tlast = '1' then
  --         dbg_ctr <= dbg_ctr + 1;
  --       end if;
  --     end if;
  --   end if;
  -- end process;

  -- fr_ctr_out <= std_logic_vector(dbg_ctr);


  p_TX_CTRL_COMB : process(tx_state, tx_fifo_ampty, tx_mac_tlast)
  begin
    next_state <= tx_state;

    case tx_state is

      when IDLE =>
        if tx_fifo_ampty = '0' then
          next_state <= PREPARE;
        else
          next_state <= IDLE;
        end if;

      when PREPARE =>
        next_state <= TRANSMITTING;

      when TRANSMITTING =>
        if tx_mac_tlast = '1' then
          next_state <= IDLE;
        --next_state <= DELAY;
        end if;

        -- when DELAY =>
        --   if fsm_ctr > 0 then
        --     fsm_ctr <= fsm_ctr - 1;
        --   else
        --     next_state <= IDLE;
        --   end if;

      when FIFO_RESET =>
        if dly_exp = '1' then
          next_state <= IDLE;
        end if;

      when others =>
        next_state <= IDLE;

    end case;
  end process;

  state_v <= x"1" when tx_state = IDLE else
             x"2" when tx_state = PREPARE else
             x"3" when tx_state = TRANSMITTING else
             x"4" when tx_state = DELAY else
             x"5" when tx_state = FIFO_RESET else
             x"E";

  p_TX_CTRL_SEQ : process(gtx_clk)
  begin
    if rising_edge(gtx_clk) then
      dly_ctr_init <= '0';
      if reset = '1' then
        tx_state     <= FIFO_RESET;
        dly_ctr_set  <= 7;
        dly_ctr_init <= '1';
      else
        tx_state <= next_state;
      end if;
    end if;
  end process;

  p_DELAY_CTR : process(gtx_clk)
  begin
    if rising_edge(gtx_clk) then
      if dly_ctr_init = '1' then
        dly_ctr <= dly_ctr_set;
        dly_exp <= '0';
      else
        if dly_ctr > 0 then
          dly_ctr <= dly_ctr - 1;
        else
          dly_exp <= '1';
        end if;
      end if;
    end if;
  end process;

------------------------------------------------------------------------------
-- Instantiate the Config vector controller Controller
------------------------------------------------------------------------------

  config_vector_controller : entity work.TEMAC_1G_CFGVEC_NOPFC_config_vector_sm
    generic map (
      RX_FC_EN => RX_FC_EN,
      TX_FC_EN => TX_FC_EN
      )
    port map (
      gtx_clk                 => gtx_clk,
      reset                   => reset,
      link_valid_in           => link_valid_in,
      mac_speed               => mac_speed,
      update_speed            => update_speed,
      mac_addr_in             => mac_addr_in,
      rx_configuration_vector => rx_configuration_vector,
      tx_configuration_vector => tx_configuration_vector,
      mac_active_out          => mac_running
      );

  mac_active_out <= mac_running;

-----------------------------------------------------------------------------
-- Instantiate the TEMAC core
-----------------------------------------------------------------------------

  THE_TEMAC_CORE : entity work.TEMAC_1G_CFGVEC_NOPFC
    port map (
      gtx_clk => gtx_clk,

      -- asynchronous reset
      glbl_rstn   => resetn,
      rx_axi_rstn => '1',               --rx_axi_rstn,
      tx_axi_rstn => '1',               --tx_axi_rstn,

      -- Receiver Interface
      ----------------------------
      rx_statistics_vector => rx_statistics_vector,
      rx_statistics_valid  => rx_statistics_valid,

      rx_mac_aclk        => rx_mac_aclk,
      rx_reset           => rx_reset,
      rx_axis_mac_tdata  => rx_mac_tdata,
      rx_axis_mac_tvalid => rx_mac_tvalid,
      rx_axis_mac_tlast  => rx_mac_tlast,
      rx_axis_mac_tuser  => rx_mac_tuser,

      -- Transmitter Interface
      -------------------------------
      tx_ifg_delay         => tx_ifg_delay,
      tx_statistics_vector => tx_statistics_vector,
      tx_statistics_valid  => tx_statistics_valid,

      tx_mac_aclk        => tx_mac_aclk,
      tx_reset           => tx_reset,
      tx_axis_mac_tdata  => tx_mac_tdata,
      tx_axis_mac_tvalid => tx_mac_tvalid,
      tx_axis_mac_tlast  => tx_mac_tlast,
      tx_axis_mac_tuser  => tx_mac_tuser,
      tx_axis_mac_tready => tx_mac_tready,

      -- MAC Control Interface
      ------------------------
      pause_req    => pause_req,
      pause_val    => pause_val,
      speedis100   => speedis100,
      speedis10100 => speedis10100,

      -- GMII Interface
      -----------------
      gmii_txd   => gmii_txd,
      gmii_tx_en => gmii_tx_en,
      gmii_tx_er => gmii_tx_er,
      gmii_rxd   => gmii_rxd,
      gmii_rx_dv => gmii_rx_dv,
      gmii_rx_er => gmii_rx_er,

      -- Configuration Vector
      -----------------------
      rx_configuration_vector => rx_configuration_vector,
      tx_configuration_vector => tx_configuration_vector
      );

  mac_rx_cfg_vec_out <= rx_configuration_vector;
  mac_tx_cfg_vec_out <= tx_configuration_vector;


  -- ila_mac_tx : entity work.ila_mac_tx
  --   port map (
  --     clk     => tx_mac_aclk,
  --     probe0  => (0 => '0'),
  --     probe1  => (0 => tx_mac_tvalid),
  --     probe2  => (0 => tx_mac_tready),
  --     probe3  => (0 => tx_mac_tlast),
  --     probe4  => tx_mac_tdata,
  --     probe5  => tx_mac_tuser,
  --     probe6  => (0 => tx_fifo_full),
  --     probe7  => (0 => tx_fifo_mpty),
  --     probe8  => (0 => tx_fifo_ampty),
  --     probe9  => state_v,
  --     probe10 => (0 => reset)
  --     );


end wrapper;

