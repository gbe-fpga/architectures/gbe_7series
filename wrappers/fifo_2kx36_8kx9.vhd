library IEEE;
use IEEE.std_logic_1164.all;


entity fifo_2kx36_8kx9 is
  port (
    Data       : in  std_logic_vector(35 downto 0);
    WrClock    : in  std_logic;
    RdClock    : in  std_logic;
    WrEn       : in  std_logic;
    RdEn       : in  std_logic;
    Reset      : in  std_logic;
    RPReset    : in  std_logic;
    Q          : out std_logic_vector(8 downto 0);
    Empty      : out std_logic;
    Full       : out std_logic;
    AlmostFull : out std_logic
    );
end fifo_2kx36_8kx9;


architecture wrapper of fifo_2kx36_8kx9 is


begin

  FIFO_2kx36_8kx9_OUTREG : entity work.fifo_ccbr_2kx36_8kx9_outreg
    port map (
      clk         => WrClock,
      rst         => Reset,
      din         => Data,
      wr_en       => WrEn,
      rd_en       => RdEn,
      dout        => Q,
      full        => Full,
      empty       => Empty,
      prog_full   => AlmostFull
      );

end wrapper;
