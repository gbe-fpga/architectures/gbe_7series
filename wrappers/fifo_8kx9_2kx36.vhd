library IEEE;
use IEEE.std_logic_1164.all;

entity fifo_8kx9_2kx36 is
  port (
    Data    : in  std_logic_vector(8 downto 0);
    WrClock : in  std_logic;
    RdClock : in  std_logic := '0';
    WrEn    : in  std_logic;
    RdEn    : in  std_logic;
    Reset   : in  std_logic;
    RPReset : in  std_logic := '0';
    Q       : out std_logic_vector(35 downto 0);
    WCNT    : out std_logic_vector(13 downto 0);
    RCNT    : out std_logic_vector(11 downto 0);
    Empty   : out std_logic;
    Full    : out std_logic);
end fifo_8kx9_2kx36;

architecture wrapper of fifo_8kx9_2kx36 is

begin

  RCNT <= (others => '0');

  FIFO_8kx9_2kx36_OUTREG_inst : entity work.fifo_ccbr_8kx9_2kx36_outreg
    port map (
      clk           => WrClock,
      rst           => Reset,
      din           => Data,
      wr_en         => WrEn,
      rd_en         => RdEn,
      dout          => Q,
      full          => Full,
      empty         => Empty,
      wr_data_count => WCNT
      );

end wrapper;
