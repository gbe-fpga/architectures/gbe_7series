library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

-- This is just a simple wrapper.

entity spi_memory is
  port(
    CLK           : in  std_logic;
    CLEAR         : in  std_logic;
    RESET         : in  std_logic;
    -- Slave bus
    BUS_ADDR_IN   : in  std_logic_vector(9 downto 0);
    BUS_WRITE_IN  : in  std_logic;
    BUS_DATA_IN   : in  std_logic_vector(15 downto 0);
    BUS_DATA_OUT  : out std_logic_vector(15 downto 0);
    -- state machine connections
    BRAM_ADDR_IN  : in  std_logic_vector(10 downto 0);
    BRAM_WR_D_OUT : out std_logic_vector(7 downto 0);
    BRAM_RD_D_IN  : in  std_logic_vector(7 downto 0);
    BRAM_WE_IN    : in  std_logic
    );
end entity spi_memory;

architecture spi_memory_arch of spi_memory is

--  attribute HGROUP : string;
--  attribute HGROUP of spi_memory_arch : architecture  is "spi_memory_group";

  signal dpram_rst : std_logic;

begin

  dpram_rst <= CLEAR or RESET;

  THE_BUS_SPI_DPRAM : entity work.blk_mem_dp_true_1kx16_2kx8
    port map (
      clka      => CLK,
      rsta      => dpram_rst,
      ena       => '1',
      wea(0)    => BUS_WRITE_IN,
      addra     => BUS_ADDR_IN,
      dina      => BUS_DATA_IN,
      douta     => BUS_DATA_OUT,
      clkb      => CLK,
      rstb      => dpram_rst,
      enb       => '1',
      web(0)    => BRAM_WE_IN,
      addrb     => BRAM_ADDR_IN,
      dinb      => BRAM_RD_D_IN,
      doutb     => BRAM_WR_D_OUT,
      rsta_busy => open,
      rstb_busy => open
      );

  -- Template from gbe_core
  -- THE_BUS_SPI_DPRAM: entity work.dp_1kx16_2kx8
  -- port map(
  --   -- A side is user access
  --   DATAINA   => BUS_DATA_IN,
  --   ADDRESSA  => BUS_ADDR_IN,
  --   CLOCKA    => CLK,
  --   CLOCKENA  => '1',
  --   WRA       => BUS_WRITE_IN,
  --   RESETA    => dpram_rst,
  --   QA        => BUS_DATA_OUT,
  --   -- B side is state machine
  --   DATAINB   => BRAM_RD_D_IN,
  --   ADDRESSB  => BRAM_ADDR_IN,
  --   CLOCKB    => CLK,
  --   CLOCKENB  => '1',
  --   WRB       => BRAM_WE_IN,
  --   RESETB    => dpram_rst,
  --   QB        => BRAM_WR_D_OUT
  -- );

end spi_memory_arch;
