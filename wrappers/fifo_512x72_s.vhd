library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;


entity fifo_512x72_s is
  port (
    Reset : in  std_logic;
    Clock : in  std_logic;
    Data  : in  std_logic_vector(71 downto 0);
    WrEn  : in  std_logic;
    RdEn  : in  std_logic;
    Q     : out std_logic_vector(71 downto 0);
    Empty : out std_logic;
    Full  : out std_logic
    );
end fifo_512x72_s;


architecture rtl of fifo_512x72_s is


begin

  FIFO_W72_D512 : entity work.FIFO_CCBI_W72_D512
    port map (
      clk       => Clock,
      rst       => Reset,
      din       => Data,
      wr_en     => WrEn,
      rd_en     => RdEn,
      dout      => Q,
      full      => open,
      overflow  => open,
      empty     => Empty,
      underflow => open,
      prog_full => Full
      );

end rtl;

