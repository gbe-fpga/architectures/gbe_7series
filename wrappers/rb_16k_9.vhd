library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;


entity rb_16k_9 is
  port (
    WRADDRESS : in  std_logic_vector(13 downto 0);
    RDADDRESS : in  std_logic_vector(13 downto 0);
    DATA      : in  std_logic_vector(8 downto 0);
    WE        : in  std_logic;
    RDCLOCK   : in  std_logic;
    RDCLOCKEN : in  std_logic;
    RESET     : in  std_logic;
    WRCLOCK   : in  std_logic;
    WRCLOCKEN : in  std_logic;
    Q         : out std_logic_vector(8 downto 0)
    );
end rb_16k_9;


architecture rtl of rb_16k_9 is

  signal wea_v, web_v : std_logic_vector(0 downto 0);

begin

  wea_v(0) <= WE;

  DP_RAM_16kx9 : entity work.blk_mem_dp_true_16kx9
    port map (
      clka  => WRCLOCK,
      ena   => WRCLOCKEN,
      wea   => wea_v,
      addra => WRADDRESS,
      dina  => DATA,
      clkb  => RDCLOCK,
      enb   => RDCLOCKEN,
      addrb => RDADDRESS,
      doutb => Q
      );


end rtl;

