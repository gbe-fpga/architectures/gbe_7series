library IEEE;
use IEEE.std_logic_1164.all;


entity fifo_dc_1kx18 is
  port (
    Data       : in  std_logic_vector(17 downto 0);
    WrClock    : in  std_logic;
    RdClock    : in  std_logic;
    WrEn       : in  std_logic;
    RdEn       : in  std_logic;
    Reset      : in  std_logic;
    RPReset    : in  std_logic;
    Q          : out std_logic_vector(17 downto 0);
    Empty      : out std_logic;
    Full       : out std_logic;
    AlmostFull : out std_logic
    );
end fifo_dc_1kx18;


architecture wrapper of fifo_dc_1kx18 is



begin

  FIFO_DC_1kx18_OUTREG : entity work.fifo_icbr_1kx18_outreg
    port map (
      rst         => Reset,
      wr_clk      => WrClock,
      rd_clk      => RdClock,
      din         => Data,
      wr_en       => WrEn,
      rd_en       => RdEn,
      dout        => Q,
      full        => Full,
      empty       => Empty,
      prog_full   => AlmostFull
      );

end wrapper;
