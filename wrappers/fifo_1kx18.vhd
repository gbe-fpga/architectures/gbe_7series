library IEEE;
use IEEE.std_logic_1164.all;

entity fifo_1kx18 is
  port (
    Data       : in  std_logic_vector(17 downto 0);
    Clock      : in  std_logic;
    WrEn       : in  std_logic;
    RdEn       : in  std_logic;
    Reset      : in  std_logic;
    Q          : out std_logic_vector(17 downto 0);
    Empty      : out std_logic;
    Full       : out std_logic;
    AlmostFull : out std_logic
    );
end fifo_1kx18;

architecture wrapper of fifo_1kx18 is

begin

  FIFO_1kx18_OUTREG_inst : entity work.fifo_ccbi_1kx18_outreg
    port map (
      clk       => Clock,
      rst       => Reset,
      din       => Data,
      wr_en     => WrEn,
      rd_en     => RdEn,
      dout      => Q,
      full      => Full,
      empty     => Empty,
      prog_full => AlmostFull
      );

end wrapper;
