library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;


entity ram_dp_true_2kx9 is
  port (
    DataInA  : in  std_logic_vector(8 downto 0);
    DataInB  : in  std_logic_vector(8 downto 0);
    AddressA : in  std_logic_vector(10 downto 0);
    AddressB : in  std_logic_vector(10 downto 0);
    ClockA   : in  std_logic;
    ClockB   : in  std_logic;
    ClockEnA : in  std_logic;
    ClockEnB : in  std_logic;
    WrA      : in  std_logic;
    WrB      : in  std_logic;
    ResetA   : in  std_logic;
    ResetB   : in  std_logic;
    QA       : out std_logic_vector(8 downto 0);
    QB       : out std_logic_vector(8 downto 0)
    );
end ram_dp_true_2kx9;


architecture rtl of ram_dp_true_2kx9 is

  signal wea_v, web_v : std_logic_vector(0 downto 0);
  
begin

  wea_v(0) <= WrA;
  web_v(0) <= WrB;

  DP_RAM_2kx9 : entity work.blk_mem_dp_true_2kx9
    port map (
      clka  => ClockA,
      rsta  => ResetA,
      ena   => ClockEnA,
      wea   => wea_v,
      addra => AddressA,
      dina  => DataInA,
      douta => QA,
      clkb  => ClockB,
      rstb  => ResetB,
      enb   => ClockEnB,
      web   => web_v,
      addrb => AddressB,
      dinb  => DataInB,
      doutb => QB
      );


end rtl;

